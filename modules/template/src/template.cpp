/*
  Template class
 */

#include "template.h"

// Includes
#include "logger.h"
#define LOG_TAG "Template"

namespace threemonkeybits
{
    bool Template::foo(int fooInt)
    {
        return true;
    }
}
