//============================================================================
// Name        : communication.cpp
// Author      : 
// Version     :
// Copyright   : LGPL
// Description : Version & Compile preprocessor
//============================================================================

// Logs
#include "GUCpp_config.h"	// Version Header
#include "logger.h"
#define LOG_TAG	"Sample-Communication"

#include "communication.h"

using namespace threemonkeybits;

// Main
int main()
{
	LOGI("Welcome to the communication sample");
	LOGI("------------------------------------------------------------------");

	return 0;
}

