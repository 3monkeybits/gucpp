//============================================================================
// Name        : regex.cpp
// Author      : Piperoman & rdcelis
// Version     :
// Copyright   : LGPL
// Description : http://www.cplusplus.com/reference/regex/
//============================================================================

// Logs
#include "logger.h"
#define LOG_TAG	"Sample-Regex"

// System includes
#include <regex>

// Needed
#include "GUCpp_config.h"	// Version Header

using namespace std;
using namespace threemonkeybits;

int main()
{
	LOGI("Welcome to regex example");
	LOGI("------------------------------------------------------------------");

	return 0;
}
